ARCHS = arm64

include theos/makefiles/common.mk

TOOL_NAME = bitwarden
bitwarden_FILES = main.m
bitwarden_FRAMEWORKS = JavaScriptCore Security UIKit
bitwarden_CODESIGN_FLAGS = -Sentitlements.xml
bitwarden_LDFLAGS = -ledit

include $(THEOS_MAKE_PATH)/tool.mk
