#import <JavaScriptCore/JavaScriptCore.h>
#include <CommonCrypto/CommonCrypto.h>
#include <CoreFoundation/CFUserNotification.h>
#include <editline/readline.h>

enum EncryptionType {
  AesCbc256_B64,
  AesCbc128_HmacSha256_B64,
  AesCbc256_HmacSha256_B64,
  Rsa2048_OaepSha256_B64,
  Rsa2048_OaepSha1_B64,
  Rsa2048_OaepSha256_HmacSha256_B64,
  Rsa2048_OaepSha1_HmacSha256_B64,
};

@interface LoginInfo : NSObject {
  CFTypeRef _qKeys[5],_qValues[5];
}
@property(nonatomic,retain) NSData* masterKey;
@property(nonatomic,readonly) NSString* authString;
@property(nonatomic,readonly) NSString* refreshToken;
@property(nonatomic,readonly) NSDate* expireDate;
@end
@implementation LoginInfo
@synthesize masterKey=_masterKey,authString=_authString,refreshToken=_refreshToken,expireDate=_expireDate;
-(instancetype)initWithUsername:(NSString*)username {
  if((self=[super init])){
    _qKeys[0]=kSecAttrAccount;_qValues[0]=[username retain];
    _qKeys[1]=kSecAttrService;_qValues[1]=CFSTR("bitwarden");
    _qKeys[2]=kSecClass;_qValues[2]=kSecClassGenericPassword;
  }
  return self;
}
-(OSStatus)loadFromKeychain {
  _qKeys[3]=kSecReturnData;_qValues[3]=kCFBooleanTrue;
  _qKeys[4]=kSecUseOperationPrompt;_qValues[4]=CFSTR("bitwarden");
  CFDictionaryRef query=CFDictionaryCreate(NULL,_qKeys,_qValues,
   5,NULL,&kCFTypeDictionaryValueCallBacks);
  CFTypeRef result;
  OSStatus status=SecItemCopyMatching(query,&result);
  CFRelease(query);
  if(status==errSecSuccess){
    NSArray* array=[NSPropertyListSerialization propertyListWithData:(NSData*)result
     options:NSPropertyListImmutable format:NULL error:NULL];
    NSUInteger count=array.count;
    if(count<2){return errSecUnimplemented;}
    if(count>4){count=4;}
    id values[4];
    [array getObjects:values range:NSMakeRange(0,count)];
    _masterKey=[values[0] retain];
    _authString=[values[1] retain];
    if(count==4){
      _refreshToken=[values[2] retain];
      _expireDate=[values[3] retain];
    }
  }
  return status;
}
-(OSStatus)saveToKeychain {
  CFDictionaryRef query=CFDictionaryCreate(NULL,_qKeys,_qValues,
   3,NULL,&kCFTypeDictionaryValueCallBacks);
  OSStatus status=SecItemDelete(query);
  CFRelease(query);
  id values[4];
  if((values[0]=_masterKey) && (values[1]=_authString)){
    _qKeys[3]=kSecValueData;_qValues[3]=[NSPropertyListSerialization dataWithPropertyList:[NSArray
     arrayWithObjects:values count:((values[2]=_refreshToken) && (values[3]=_expireDate))?4:2]
     format:NSPropertyListBinaryFormat_v1_0 options:0 error:NULL];
    SecAccessControlRef accessControl=SecAccessControlCreateWithFlags(NULL,
     kSecAttrAccessibleWhenUnlockedThisDeviceOnly,kSecAccessControlUserPresence,NULL);
    _qKeys[4]=kSecAttrAccessControl;_qValues[4]=accessControl;
    CFDictionaryRef query=CFDictionaryCreate(NULL,_qKeys,_qValues,
     5,NULL,&kCFTypeDictionaryValueCallBacks);
    CFRelease(accessControl);
    status=SecItemAdd(query,NULL);
    CFRelease(query);
  }
  return status;
}
-(BOOL)updateWithDictionary:(NSDictionary*)dict {
  NSString* accessToken;
  NSString* tokenType=[dict objectForKey:@"token_type"];
  if(!tokenType || !(accessToken=[dict objectForKey:@"access_token"])){return NO;}
  [_authString release];
  _authString=[[NSString alloc] initWithFormat:@"%@ %@",tokenType,accessToken];
  NSString* refreshToken=[dict objectForKey:@"refresh_token"];
  if(refreshToken){
    [_refreshToken release];
    _refreshToken=[refreshToken retain];
  }
  NSString* expires=[dict objectForKey:@"expires_in"];
  if(expires){
    [_expireDate release];
    _expireDate=[[NSDate alloc] initWithTimeIntervalSinceNow:expires.doubleValue];
  }
  return YES;
}
-(void)dealloc {
  [(NSString*)_qValues[0] release];
  [_masterKey release];
  [_authString release];
  [_refreshToken release];
  [_expireDate release];
  [super dealloc];
}
@end

static NSMutableURLRequest* loginRequest(NSString* bodyfmt,...){
  NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:
   [NSURL URLWithString:@"https://identity.bitwarden.com/connect/token"]];
  request.HTTPMethod=@"POST";
  [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
  NSMutableCharacterSet* allowed=[NSCharacterSet URLQueryAllowedCharacterSet].mutableCopy;
  [allowed removeCharactersInString:@"&+="];
  va_list args;
  va_start(args,bodyfmt);
  NSMutableString* body=bodyfmt.mutableCopy;
  NSRange remain=NSMakeRange(0,body.length);
  while(1){
    NSRange match=[body rangeOfString:@"%@" options:0 range:remain];
    if(match.location==NSNotFound){break;}
    NSString* param=[va_arg(args,NSString*)
     stringByAddingPercentEncodingWithAllowedCharacters:allowed];
    [body replaceCharactersInRange:match withString:param];
    remain.length-=match.location+match.length-remain.location;
    remain.location=match.location+param.length;
  }
  va_end(args);
  [allowed release];
  request.HTTPBody=[body dataUsingEncoding:NSUTF8StringEncoding];
  [body release];
  return request;
}
static NSDictionary* doJSONRequest(NSURLSession* session,NSURLRequest* request){
  __block NSDictionary* dict=nil;
  CFRunLoopRef runloop=CFRunLoopGetCurrent();
  [[session dataTaskWithRequest:request completionHandler:^(NSData* data,NSURLResponse* response,NSError* error){
    int statusCode=((NSHTTPURLResponse*)response).statusCode;
    if(statusCode==200 && [dict=[NSJSONSerialization
     JSONObjectWithData:data options:0 error:&error]
     isKindOfClass:[NSDictionary class]]){dict=[dict retain];}
    else {
      fprintf(stderr,"W: HTTP %d",statusCode);
      if(error){fprintf(stderr," (%s)",error.localizedDescription.UTF8String);}
      if(data.length){
        fputs(" -- ",stderr);
        fwrite(data.bytes,1,data.length,stderr);
      }
      fputc('\n',stderr);
    }
    CFRunLoopStop(runloop);
  }] resume];
  CFRunLoopRun();
  return [dict autorelease];
}
static NSData* extractPKCS1(NSData* PKCS8) {
  if(!PKCS8){return nil;}
  const uint8_t* ptr=PKCS8.bytes;
  const uint8_t* end=ptr+PKCS8.length;
  if(ptr+2>end || *ptr!=0x30){return nil;}
  uint8_t v=*++ptr;
  ptr+=1+((v&0x80)?v&0x7f:1);
  const uint8_t match[]={0x02,0x01,0x00,0x30,0x0d,0x06,0x09,
   0x2a,0x86,0x48,0x86,0xf7,0x0d,0x01,0x01,0x01,0x05,0x00,0x04};
  if(ptr+sizeof(match)+1>end){return nil;}
  unsigned int i;
  for (i=0;i<sizeof(match);i++){
    if(*ptr!=match[i]){return nil;}
    ptr++;
  }
  size_t size=*ptr++;
  if(size&0x80){
    uint8_t v=size&0x7f;
    if(ptr+v>end){return nil;}
    size=0;
    while(v){
      size=(size<<8)|*ptr++;
      v--;
    }
  }
  if(ptr+size>end){return nil;}
  return [NSData dataWithBytes:ptr length:size];
}
typedef uint8_t* (*HashFunc)(const void* data,CC_LONG len,uint8_t* md);
static void MGF1(HashFunc hfunc,CC_LONG hlen,uint8_t* inbuf,CC_LONG inlen,uint8_t* outbuf,CC_LONG outlen) {
  uint32_t* cptr=(uint32_t*)(inbuf+inlen);
  uint32_t save=*cptr,counter=0;
  inlen+=sizeof(save);
  uint8_t* outend=outbuf+outlen;
  while(outbuf<outend){
    *cptr=OSSwapHostToBigInt32(counter++);
    hfunc(inbuf,inlen,outbuf);
    outbuf+=hlen;
  }
  *cptr=save;
}
static NSData* rsaDecrypt(NSString* cipherString,SecKeyRef privateKey){
  const CC_LONG blocksize=2048/8;
  if(![cipherString isKindOfClass:[NSString class]]
   || SecKeyGetBlockSize(privateKey)!=blocksize){return nil;}
  HashFunc hfunc;
  CC_LONG hlen;
  NSArray* pieces=[cipherString componentsSeparatedByString:@"."];
  switch(pieces.count){
    case 2:{
      switch(((NSNumber*)[pieces objectAtIndex:0]).intValue){
        case Rsa2048_OaepSha256_B64:
        case Rsa2048_OaepSha256_HmacSha256_B64://deprecated
          hfunc=CC_SHA256;hlen=CC_SHA256_DIGEST_LENGTH;break;
        case Rsa2048_OaepSha1_B64:
        case Rsa2048_OaepSha1_HmacSha256_B64://deprecated
          hfunc=CC_SHA1;hlen=CC_SHA1_DIGEST_LENGTH;break;
        default:return nil;
      }
      pieces=[[pieces objectAtIndex:1] componentsSeparatedByString:@"|"];
      break;
    }
    case 1:{
      pieces=[cipherString componentsSeparatedByString:@"|"];
      hfunc=CC_SHA256;hlen=CC_SHA256_DIGEST_LENGTH;break;
    }
    default:return nil;
  }
  NSData* ptData=nil;
  NSData* ctData=[[NSData alloc] initWithBase64EncodedString:[pieces objectAtIndex:0]
   options:NSDataBase64DecodingIgnoreUnknownCharacters];
  uint8_t* buf=malloc(blocksize+4);
  size_t len=blocksize;
  // RSAES-OAEP (RFC 8017)
  if(SecKeyDecrypt(privateKey,kSecPaddingNone,ctData.bytes,ctData.length,
   buf,&len)==errSecSuccess && len==blocksize-1){
    uint8_t mask[blocksize];
    CC_LONG dlen=len-hlen;
    uint8_t* optr=buf;
    MGF1(hfunc,hlen,buf+hlen,dlen,mask,hlen);
    for (CC_LONG i=0;i<hlen;i++){*optr++^=mask[i];}
    MGF1(hfunc,hlen,buf,hlen,mask,dlen);
    hfunc(NULL,0,buf);
    uint8_t* mptr=NULL;
    volatile uint8_t vresult=0;
    for (CC_LONG i=0;i<dlen;i++){
      uint8_t v=*optr++^=mask[i];
      if(mptr){continue;}
      if(i<hlen){vresult|=v^buf[i];}
      else if(v){vresult|=v^1;mptr=optr;}
    }
    if(vresult==0 && mptr){ptData=[NSData dataWithBytes:mptr length:buf+len-mptr];}
  }
  free(buf);
  [ctData release];
  return ptData;
}
static NSData* aesDecrypt(NSString* cipherString,NSData* aesKey,BOOL canStretch){
  if(![cipherString isKindOfClass:[NSString class]]){return nil;}
  int keysize,verify;
  NSArray* pieces=[cipherString componentsSeparatedByString:@"."];
  switch(pieces.count){
    case 2:{
      switch(((NSNumber*)[pieces objectAtIndex:0]).intValue){
        case AesCbc256_B64:keysize=kCCKeySizeAES256;verify=0;break;
        case AesCbc128_HmacSha256_B64:keysize=kCCKeySizeAES128;verify=1;break;
        case AesCbc256_HmacSha256_B64:keysize=kCCKeySizeAES256;verify=canStretch?2:1;break;
        default:return nil;
      }
      pieces=[[pieces objectAtIndex:1] componentsSeparatedByString:@"|"];
      if(pieces.count!=(verify?3:2)){return nil;}
      break;
    }
    case 1:{
      pieces=[cipherString componentsSeparatedByString:@"|"];
      switch(pieces.count){
        case 3:keysize=kCCKeySizeAES128;verify=1;break;
        case 2:keysize=kCCKeySizeAES256;verify=0;break;
        default:return nil;
      }
      break;
    }
    default:return nil;
  }
  if(verify==2){
    const uint8_t* ptr=aesKey.bytes;
    const size_t len=aesKey.length;
    const size_t bufsize=2*keysize;
    uint8_t* buf=malloc(bufsize);
    // HKDF-Expand (RFC 5869)
    CCHmac(kCCHmacAlgSHA256,ptr,len,(uint8_t[]){'e','n','c',1},4,buf);
    CCHmac(kCCHmacAlgSHA256,ptr,len,(uint8_t[]){'m','a','c',1},4,buf+keysize);
    aesKey=[NSData dataWithBytesNoCopy:buf length:bufsize];
  }
  else if(aesKey.length!=(verify?2*keysize:keysize)){return nil;}
  NSData* ptData=nil;
  NSData* ivData=[[NSData alloc] initWithBase64EncodedString:[pieces objectAtIndex:0]
   options:NSDataBase64DecodingIgnoreUnknownCharacters];
  if(ivData.length==kCCBlockSizeAES128){
    NSData* ctData=[[NSData alloc] initWithBase64EncodedString:[pieces objectAtIndex:1]
     options:NSDataBase64DecodingIgnoreUnknownCharacters];
    if(verify){
      NSData* macData=[[NSData alloc] initWithBase64EncodedString:[pieces objectAtIndex:2]
       options:NSDataBase64DecodingIgnoreUnknownCharacters];
      if(macData.length==CC_SHA256_DIGEST_LENGTH){
        CCHmacContext context;
        uint8_t hmac[CC_SHA256_DIGEST_LENGTH];
        CCHmacInit(&context,kCCHmacAlgSHA256,aesKey.bytes+keysize,keysize);
        CCHmacUpdate(&context,ivData.bytes,kCCBlockSizeAES128);
        CCHmacUpdate(&context,ctData.bytes,ctData.length);
        CCHmacFinal(&context,hmac);
        const uint8_t* vptr=macData.bytes;
        volatile uint8_t vresult=0;
        for (int i=0;i<sizeof(hmac);i++){vresult|=hmac[i]^vptr[i];}
        if(vresult==0){verify=0;}
      }
      [macData release];
    }
    if(!verify){
      size_t len=ctData.length;
      uint8_t* buf=malloc(len);
      if(CCCrypt(kCCDecrypt,kCCAlgorithmAES128,kCCOptionPKCS7Padding,
       aesKey.bytes,keysize,ivData.bytes,ctData.bytes,len,buf,len,&len)==kCCSuccess){
        ptData=[NSData dataWithBytesNoCopy:buf length:len];
      }
      else {free(buf);}
    }
    [ctData release];
  }
  [ivData release];
  return ptData;
}
static id aesDecryptAll(id object,NSData* aesKey,NSDictionary* organizationKeys,NSCache* cache){
  BOOL isDictionary;
  if([object isKindOfClass:[NSString class]]){
    NSData* data;
    NSString* value=[cache objectForKey:object];
    if(value){object=value;}
    else if((data=aesDecrypt(object,aesKey,NO))
     && (value=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding])){
      [cache setObject:value forKey:object];
      object=[value autorelease];
    }
  }
  else if((isDictionary=[object isKindOfClass:[NSDictionary class]])
   || [object isKindOfClass:[NSArray class]]){
    BOOL changed=NO;
    NSCache* newcache=nil;
    const NSUInteger count=((NSDictionary*)object).count;
    id* keys;
    id* values=malloc(count*sizeof(*values));
    if(isDictionary){
      [object getObjects:values andKeys:keys=malloc(count*sizeof(*keys))];
      NSString* organizationId=[object objectForKey:@"organizationId"];
      if(organizationId){
        NSData* organizationKey=[organizationKeys objectForKey:organizationId];
        if(organizationKey){
          aesKey=organizationKey;
          cache=newcache=[[NSCache alloc] init];
        }
      }
    }
    else {[object getObjects:values range:NSMakeRange(0,count)];}
    for (NSUInteger i=0;i<count;i++){
      id value=aesDecryptAll(values[i],aesKey,organizationKeys,cache);
      if(values[i]==value){continue;}
      values[i]=value;
      changed=YES;
    }
    [newcache release];
    if(changed){
      object=isDictionary?[NSDictionary dictionaryWithObjects:values forKeys:keys count:count]:
       [NSArray arrayWithObjects:values count:count];
    }
    if(isDictionary){free(keys);}
    free(values);
  }
  return object;
}
NSString* base64URLEncode(NSString* string) {
  NSData* data=[[string dataUsingEncoding:NSUTF8StringEncoding] base64EncodedDataWithOptions:0];
  const char* bytes=data.bytes;
  NSUInteger length=data.length;
  char* cstr=malloc(length+1);
  for (NSUInteger i=0;i<length;i++){
    char c=bytes[i];
    if(c=='='){length=i;break;}
    cstr[i]=(c=='+')?'-':(c=='/')?'_':c;
  }
  string=[[NSString alloc] initWithBytes:cstr length:length encoding:NSUTF8StringEncoding];
  free(cstr);
  return string;
}

int main(int argc,char** argv) {@autoreleasepool {
  if(argc<2){goto __showUsage;}
  int logout;
  if(argc==2){logout=0;}
  else if(strcmp(argv[2],"logout")==0){logout=1;}
  else __showUsage:{
    fprintf(stderr,"Usage: %s <username> [logout]\n",argv[0]);
    return -1;
  }
  char* user=argv[1];
  NSString* username=[NSString stringWithUTF8String:user];
  LoginInfo* loginInfo=[[LoginInfo alloc] initWithUsername:username];
  CFTypeRef kqkeys[7],kqvalues[7];
  kqkeys[0]=kSecClass;kqvalues[0]=kSecClassKey;
  kqkeys[1]=kSecAttrKeyClass;kqvalues[1]=kSecAttrKeyClassPrivate;
  kqkeys[2]=kSecAttrKeyType;kqvalues[2]=kSecAttrKeyTypeRSA;
  kqkeys[3]=kSecAttrApplicationTag;kqvalues[3]=[@"bitwarden:" stringByAppendingString:username];
  if(logout){
    OSStatus status=[loginInfo saveToKeychain];
    if(status!=errSecSuccess){fprintf(stderr,"W: SecItemDelete(login) => %d\n",(int)status);}
    CFDictionaryRef query=CFDictionaryCreate(NULL,kqkeys,kqvalues,
     4,NULL,&kCFTypeDictionaryValueCallBacks);
    status=SecItemDelete(query);
    CFRelease(query);
    if(status!=errSecSuccess){fprintf(stderr,"W: SecItemDelete(key) => %d\n",(int)status);}
  }
  else {
    kqkeys[4]=kSecReturnRef;kqvalues[4]=kCFBooleanTrue;
    NSURLSession* session=[NSURLSession sessionWithConfiguration:
     [NSURLSessionConfiguration ephemeralSessionConfiguration]];
    SecKeyRef privateKey=NULL;
    OSStatus status=[loginInfo loadFromKeychain];
    if(status==errSecSuccess){
      NSString* refreshToken=loginInfo.refreshToken;
      if(refreshToken && loginInfo.expireDate.timeIntervalSinceNow<5){
        fputs("Reauthenticating...\n",stderr);
        NSDictionary* dict=doJSONRequest(session,loginRequest(
         @"client_id=mobile&grant_type=refresh_token&refresh_token=%@",refreshToken));
        if([loginInfo updateWithDictionary:dict]){
          OSStatus status=[loginInfo saveToKeychain];
          if(status!=errSecSuccess){fprintf(stderr,"E: SecItemAdd(login) => %d\n",(int)status);}
        }
        else {fprintf(stderr,"W: RefreshToken => %s\n",dict.description.UTF8String);}
      }
      CFDictionaryRef query=CFDictionaryCreate(NULL,kqkeys,kqvalues,
       5,NULL,&kCFTypeDictionaryValueCallBacks);
      OSStatus status=SecItemCopyMatching(query,(CFTypeRef*)&privateKey);
      CFRelease(query);
      if(status!=errSecSuccess){fprintf(stderr,"W: SecItemCopyMatching(key) => %d\n",(int)status);}
    }
    else if(status==errSecItemNotFound){
      fputs("Requesting login...\n",stderr);
      NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:
       [NSURL URLWithString:@"https://api.bitwarden.com/accounts/prelogin"]];
      request.HTTPMethod=@"POST";
      [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
      request.HTTPBody=[NSJSONSerialization dataWithJSONObject:@{@"email":username}
       options:0 error:NULL];
      NSDictionary* dict=doJSONRequest(session,request);
      const int kdf=((NSNumber*)[dict objectForKey:@"kdf"]).intValue;
      if(kdf || [[dict objectForKey:@"object"] isEqualToString:@"error"]){
        fprintf(stderr,"E: Unhandled response (kdf=%d)\n",kdf);
      }
      else {
        CFArrayRef titles=CFArrayCreate(NULL,
         (CFTypeRef[]){CFSTR("Password"),CFSTR("Token")},2,NULL);
        CFDictionaryRef attrs=CFDictionaryCreate(NULL,
         (CFTypeRef[]){kCFUserNotificationAlertHeaderKey,kCFUserNotificationAlertMessageKey,
         kCFUserNotificationTextFieldTitlesKey,kCFUserNotificationAlternateButtonTitleKey},
         (CFTypeRef[]){CFSTR("bitwarden"),username,titles,CFSTR("Cancel")},
         4,NULL,&kCFTypeDictionaryValueCallBacks);
        CFRelease(titles);
        CFUserNotificationRef prompt=CFUserNotificationCreate(NULL,0,
         kCFUserNotificationPlainAlertLevel|CFUserNotificationSecureTextField(0),NULL,attrs);
        CFRelease(attrs);
        CFOptionFlags response;
        if(CFUserNotificationReceiveResponse(prompt,0,&response)==0
         && (response&3)==kCFUserNotificationDefaultResponse){
          CFArrayRef values=CFDictionaryGetValue(CFUserNotificationGetResponseDictionary(prompt),
           kCFUserNotificationTextFieldValuesKey);
          const char* pwstr=((NSString*)CFArrayGetValueAtIndex(values,0)).UTF8String;
          const size_t pwlen=strlen(pwstr);
          uint8_t key[kCCKeySizeAES256];
          const int nrounds=((NSNumber*)[dict objectForKey:@"kdfIterations"]).intValue;
          CCKeyDerivationPBKDF(kCCPBKDF2,pwstr,pwlen,(uint8_t*)user,strlen(user),
           kCCPRFHmacAlgSHA256,(nrounds<5000)?5000:nrounds,(uint8_t*)key,sizeof(key));
          uint8_t hash[CC_SHA256_DIGEST_LENGTH];
          CCKeyDerivationPBKDF(kCCPBKDF2,(char*)key,sizeof(key),(uint8_t*)pwstr,pwlen,
           kCCPRFHmacAlgSHA256,1,hash,sizeof(hash));
          fputs("Authenticating...\n",stderr);
          NSMutableURLRequest* request=loginRequest(
           @"scope=api+offline_access&client_id=mobile&grant_type=password&username=%@&password=%@"
           "&deviceType=1&deviceIdentifier=00000000-0000-0000-0000-000000000000&deviceName=iOS"
           "&twoFactorToken=%@&twoFactorProvider=0&twoFactorRemember=0",
           username,[[NSData dataWithBytesNoCopy:hash length:sizeof(hash) freeWhenDone:NO]
           base64EncodedStringWithOptions:0],CFArrayGetValueAtIndex(values,1));
          [request setValue:base64URLEncode(username) forHTTPHeaderField:@"Auth-Email"];
          NSDictionary* dict=doJSONRequest(session,request);
          int value;
          if((value=((NSNumber*)[dict objectForKey:@"ForcePasswordReset"]).intValue))
            fprintf(stderr,"W: ForcePasswordReset = %d\n",value);
          if((value=((NSNumber*)[dict objectForKey:@"ResetMasterPassword"]).intValue))
            fprintf(stderr,"W: ResetMasterPassword = %d\n",value);
          if([loginInfo updateWithDictionary:dict]){
            NSData* derivedKey=[NSData dataWithBytes:key length:sizeof(key)];
            NSData* masterKey=aesDecrypt([dict objectForKey:@"Key"],derivedKey,YES);
            if(!masterKey){masterKey=derivedKey;}
            loginInfo.masterKey=masterKey;
            OSStatus status=[loginInfo saveToKeychain];
            if(status!=errSecSuccess){fprintf(stderr,"E: SecItemAdd(login) => %d\n",(int)status);}
            NSData* pkdata=extractPKCS1(aesDecrypt([dict objectForKey:@"PrivateKey"],masterKey,NO));
            if(pkdata){
              CFDictionaryRef query=CFDictionaryCreate(NULL,kqkeys,kqvalues,
               4,NULL,&kCFTypeDictionaryValueCallBacks);
              SecItemDelete(query);
              CFRelease(query);
              kqkeys[5]=kSecValueData;kqvalues[5]=pkdata;
              kqkeys[6]=kSecAttrAccessible;kqvalues[6]=kSecAttrAccessibleWhenUnlockedThisDeviceOnly;
              query=CFDictionaryCreate(NULL,kqkeys,kqvalues,7,NULL,&kCFTypeDictionaryValueCallBacks);
              OSStatus status=SecItemAdd(query,(CFTypeRef*)&privateKey);
              CFRelease(query);
              if(status!=errSecSuccess){fprintf(stderr,"E: SecItemAdd(key) => %d\n",(int)status);}
            }
            else {fputs("W: !key\n",stderr);}
          }
          else {fprintf(stderr,"E: GetToken => %s\n",dict.description.UTF8String);}
        }
        CFRelease(prompt);
      }
    }
    else {fprintf(stderr,"E: SecItemCopyMatching(login) => %d\n",(int)status);}
    NSData* masterKey=loginInfo.masterKey;
    if(masterKey){
      fputs("Loading data...\n",stderr);
      NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:
       [NSURL URLWithString:@"https://api.bitwarden.com/sync?excludeDomains=true"]];
      [request setValue:loginInfo.authString forHTTPHeaderField:@"Authorization"];
      NSDictionary* dict=doJSONRequest(session,request);
      NSDictionary* organizationKeys;
      if(privateKey){
        NSArray* organizations=[[dict objectForKey:@"profile"] objectForKey:@"organizations"];
        NSMutableDictionary* mKeys=[NSMutableDictionary dictionaryWithCapacity:organizations.count];
        for (NSDictionary* organization in organizations){
          NSString* organizationId=[organization objectForKey:@"id"];
          if(!organizationId){continue;}
          NSData* organizationKey=rsaDecrypt([organization objectForKey:@"key"],privateKey);
          if(organizationKey){[mKeys setObject:organizationKey forKey:organizationId];}
        }
        organizationKeys=mKeys;
        CFRelease(privateKey);
      }
      else {organizationKeys=nil;}
      NSCache* cache=[[NSCache alloc] init];
      JSContext* jsContext=[[JSContext alloc] init];
      NSString* const exports[]={@"ciphers",@"folders",@"collections"};
      for (int i=0;i<sizeof(exports)/sizeof(*exports);i++){
        id object=aesDecryptAll([dict objectForKey:exports[i]],masterKey,organizationKeys,cache);
        if(object){[jsContext setObject:object forKeyedSubscript:exports[i]];}
      }
      [cache release];
      UIPasteboard* pasteboard=[UIPasteboard generalPasteboard];
      [jsContext setObject:^(JSValue* value){
        [pasteboard setValue:value.isUndefined?@"":value.toString
         forPasteboardType:@"public.utf8-plain-text"];
      } forKeyedSubscript:@"copy"];
      [jsContext setObject:^(JSValue* value){
        if(!value.isUndefined){fputs(value.toString.UTF8String,stdout);}
        fputc('\n',stdout);
      } forKeyedSubscript:@"print"];
      [jsContext setObject:^JSValue*(JSValue* value){
        if(value.isUndefined){return nil;}
        NSString* script=[NSString stringWithContentsOfFile:value.toString
         encoding:NSUTF8StringEncoding error:nil];
        if(!script){return nil;}
        return [value.context evaluateScript:script];
      } forKeyedSubscript:@"load"];
      [jsContext setObject:^JSValue*(JSValue* input){
        NSArray* passwords=input.isUndefined?nil:
         input.isObject?input.toArray:@[input.toString];
        NSUInteger count=passwords.count;
        if(!count){return nil;}
        NSMutableDictionary* prefixes=[NSMutableDictionary dictionaryWithCapacity:count];
        const char* const hexdigits="0123456789ABCDEF";
        for (NSString* password in passwords){
          const char* str=password.UTF8String;
          unsigned char digest[2*CC_SHA1_DIGEST_LENGTH];
          CC_SHA1(str,strlen(str),digest+CC_SHA1_DIGEST_LENGTH);
          for (int i=0;i<CC_SHA1_DIGEST_LENGTH;i++){
            unsigned char c=digest[i+CC_SHA1_DIGEST_LENGTH];
            digest[2*i]=hexdigits[c>>4];
            digest[2*i+1]=hexdigits[c&0xF];
          }
          NSString* prefix=[[[NSString alloc] initWithBytes:digest length:5
           encoding:NSASCIIStringEncoding] autorelease];
          NSData* suffix=[NSData dataWithBytes:digest+5 length:sizeof(digest)-5];
          NSMutableDictionary* suffixes=[prefixes objectForKey:prefix];
          if(suffixes){
            NSString* saved=[suffixes objectForKey:suffix];
            if(saved && ![saved isEqualToString:password]){
              fputs("SHA-1 collision: ",stderr);
              fwrite(digest,1,sizeof(digest),stderr);
              fputc('\n',stderr);
            }
          }
          else {[prefixes setObject:suffixes=[NSMutableDictionary dictionary] forKey:prefix];}
          [suffixes setObject:password forKey:suffix];
        }
        __block NSUInteger remain=prefixes.count;
        NSMutableDictionary* results=[NSMutableDictionary dictionaryWithCapacity:remain];
        CFRunLoopRef runloop=CFRunLoopGetCurrent();
        for (NSString* prefix in prefixes){
          [[session dataTaskWithURL:[NSURL URLWithString:
           [@"https://api.pwnedpasswords.com/range/" stringByAppendingString:prefix]]
           completionHandler:^(NSData* data,NSURLResponse* response,NSError* error){
            int statusCode;
            if(error){
              fprintf(stderr,"<%s> E: %s\n",prefix.UTF8String,
               error.localizedDescription.UTF8String);
            }
            else if((statusCode=((NSHTTPURLResponse*)response).statusCode)!=200){
              fprintf(stderr,"<%s> HTTP %d\n",prefix.UTF8String,statusCode);
            }
            if(data){
              NSDictionary* suffixes=[prefixes objectForKey:prefix];
              const char* ptr=data.bytes;
              const char* end=ptr+data.length;
              while(ptr<end){
                const char* nl=memchr(ptr,'\n',end-ptr)?:end;
                const char* sep=memchr(ptr,':',nl-ptr);
                NSString* password=[suffixes objectForKey:[NSData
                 dataWithBytesNoCopy:(char*)ptr length:(sep?:nl)-ptr freeWhenDone:NO]];
                if(password){
                  id value;
                  if(sep){
                    NSUInteger ivalue=0;
                    while(++sep<nl && *sep>='0' && *sep<='9')
                      ivalue=10*ivalue+*sep-'0';
                    value=[NSNumber numberWithUnsignedInteger:ivalue];
                  }
                  else {value=[NSNull null];}
                  [results setObject:value forKey:password];
                }
                ptr=nl+1;
              }
            }
            if(!--remain){CFRunLoopStop(runloop);}
          }] resume];
        }
        CFRunLoopRun();
        return [JSValue valueWithObject:results inContext:input.context];
      } forKeyedSubscript:@"lookup"];
      jsContext.exceptionHandler=^(JSContext* jsContext,JSValue* exception){
        fprintf(stderr,"%s\n",exception.toString.UTF8String);
      };
      JSValue* jsFunction=[jsContext objectForKeyedSubscript:@"Function"];
      rl_bind_key('\t',rl_insert);
      BOOL init=YES;
      while(1){@autoreleasepool {
        NSString* script;
        if(init){
          init=NO;
          script=[NSString stringWithContentsOfFile:
           [NSHomeDirectory() stringByAppendingPathComponent:@".bitwardenrc"]
           encoding:NSUTF8StringEncoding error:NULL];
          if(!script.length){continue;}
        }
        else {
          char* line=readline(">>> ");
          if(!line){break;}
          if((script=*line?[NSString stringWithUTF8String:line]:nil)){add_history(line);}
          free(line);
          if(!script){continue;}
        }
        JSValue* jsValue=[jsContext evaluateScript:script];
        if(jsValue.isUndefined){continue;}
        [jsContext setObject:jsValue forKeyedSubscript:@"$0"];
        id object=(jsValue.isObject && ![jsValue isInstanceOf:jsFunction])?jsValue.toObject:nil;
        if(object && [NSJSONSerialization isValidJSONObject:object]){
          NSData* data=[NSJSONSerialization dataWithJSONObject:object
           options:NSJSONWritingPrettyPrinted error:NULL];
          fwrite(data.bytes,1,data.length,stdout);
          fputc('\n',stdout);
        }
        else {printf("%s\n",jsValue.toString.UTF8String);}
      }}
      [jsContext release];
    }
    [session invalidateAndCancel];
  }
  [loginInfo release];
}}
